﻿using Battleships.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.UI
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var guesses = new List<string>();
            var grid = new Grid(10, 10);

            PrintShipCoordinates(grid);

            while (grid.AllShipsSunk())
            {
                PrintShipStatus(grid);
                Console.WriteLine("Please enter coordinates to attack (e.g. 2,5)");

                var input = Console.ReadLine();

                try
                {
                    var inputCoordinates = input.Split(',');
                    var gridReference = new GridReference(char.Parse(inputCoordinates[0]), int.Parse(inputCoordinates[1]));

                    if (guesses.Any(x => x.Equals(input)))
                    {
                        Console.WriteLine("You've already guessed that one!");
                    }
                    else
                    {
                        var shotResult = grid.AttackGridReference(gridReference);
                        Console.WriteLine($"{gridReference.ToString()} result is...{shotResult}!");

                        guesses.Add(input);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error caught: {0}", ex.Message);
                }
            }

            Console.WriteLine($"All ships sunk! It only took {guesses.Count()} attempts (you can do better!)");
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();
        }

        private static void PrintShipStatus(Grid grid)
        {
            Console.WriteLine($"{string.Join(", ", grid.Ships.Select(x => $"{x.ShipType} = {x.Health}"))}");
        }

        private static void PrintShipCoordinates(Grid grid)
        {
            Console.WriteLine($"{string.Join(Environment.NewLine, grid.Ships.Select(x => $"{x.ShipType} = {string.Join(",", x.GridLocations.Select(y => y.ToString()))}"))}");
        }
    }
}