﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleships.Crosscutting
{
    public abstract class ShotSubject
    {
        private List<ShotObserver> _observers = new List<ShotObserver>();

        public void Attach(ShotObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(ShotObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify(GridReference gridReference)
        {
            foreach (ShotObserver o in _observers)
            {
                o.UpdateAttackedGridRef(gridReference.XCoordinate, gridReference.YCoordinate);
            }
        }
    }
}
