﻿namespace Battleships.Crosscutting
{
    public abstract class ShotObserver
    {
        public abstract void UpdateAttackedGridRef(GridReference gridReference);
    }
}