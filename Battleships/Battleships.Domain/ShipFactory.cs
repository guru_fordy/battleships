﻿using System;

namespace Battleships.Domain
{
    public static class ShipFactory
    {
        public static Ship CreateShip(ShipType shipType, ShipDirection shipDirection)
        {
            switch (shipType)
            {
                case ShipType.Battleship:
                    return new Battleship(shipDirection);

                case ShipType.Destroyer:
                    return new Destroyer(shipDirection);

                default: throw new ArgumentOutOfRangeException("shipType");
            }
        }
    }
}