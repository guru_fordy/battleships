﻿namespace Battleships.Domain
{
    public enum ShipDirection
    {
        Horizontal,
        Vertical
    }
}
