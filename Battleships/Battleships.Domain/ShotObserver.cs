﻿namespace Battleships.Domain
{
    public abstract class ShotObserver
    {
        public abstract Ship UpdateAttackedGridRef(GridReference gridReference);
    }
}