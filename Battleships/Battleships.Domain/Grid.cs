﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleships.Domain
{
    public class Grid : ShotSubject
    {
        private int[,] _gridReferences;

        public int Height { get; private set; }
        public IList<Ship> Ships { get; private set; }
        public int Width { get; private set; }

        public Grid(int height, int width)
        {
            Height = height;
            Width = width;
            Ships = new List<Ship>();
            _gridReferences = new int[height, width];

            PlaceShips(1, 2);
        }

        public bool AllShipsSunk()
        {
            return Ships.Any(x => x.IsSunk == false);
        }

        public GridReferenceStatus AttackGridReference(GridReference gridReference)
        {
            var currentStatus = GetCellStatus(gridReference);

            switch (currentStatus)
            {
                case GridReferenceStatus.Sea:
                    SetGridReferenceStatus(gridReference, GridReferenceStatus.Missed);
                    return GridReferenceStatus.Missed;

                case GridReferenceStatus.ShipPart:
                    var isSunk = Notify(gridReference).IsSunk;
                    var gridReferenceStatus = isSunk ? GridReferenceStatus.Sunk : GridReferenceStatus.HitOnShip;
                    SetGridReferenceStatus(gridReference, gridReferenceStatus);
                    return gridReferenceStatus;

                default: throw new ApplicationException("We shouldn't be able to attack an already attacked grid reference");
            }
        }

        public GridReferenceStatus GetCellStatus(GridReference gridReference)
        {
            return (GridReferenceStatus)_gridReferences[gridReference.XCoordinate, gridReference.YCoordinate];
        }

        public void SetGridReferenceStatus(GridReference gridReference, GridReferenceStatus shipPart)
        {
            _gridReferences[gridReference.XCoordinate, gridReference.YCoordinate] = (int)shipPart;
        }

        private Ship CreateShip(ShipType shipType)
        {
            ShipDirection shipDirection = GetRandomShipDirection();
            var ship = ShipFactory.CreateShip(shipType, shipDirection);

            do
            {
                var startingGridRef = GetRandomStartingPlace(ship);
                ship.PlaceShipOnGrid(this, startingGridRef);
            } while (!ship.IsPlaced());
            return ship;
        }

        private ShipDirection GetRandomShipDirection()
        {
            var random = new Random();
            return random.Next(0, 2) == 0 ? ShipDirection.Vertical : ShipDirection.Horizontal;
        }

        private GridReference GetRandomStartingPlace(Ship ship)
        {
            int xCoordinate;
            int yCoordinate;
            var random = new Random();
            if (ship.ShipDirection == ShipDirection.Vertical)
            {
                xCoordinate = random.Next(0, Width);
                yCoordinate = random.Next(1, Height - ship.Size);
            }
            else
            {
                xCoordinate = random.Next(0, Width - ship.Size);
                yCoordinate = random.Next(1, Height);
            }
            return new GridReference(xCoordinate, yCoordinate);
        }

        private void PlaceShips(int numberOfBattleships, int numberOfDestroyers)
        {
            for (int i = 1; i <= numberOfBattleships; i++)
            {
                var ship = CreateShip(ShipType.Battleship);
                Ships.Add(ship);
                Attach(ship);
            }

            for (int i = 1; i <= numberOfDestroyers; i++)
            {
                var ship = CreateShip(ShipType.Destroyer);
                Ships.Add(ship);
                Attach(ship);
            }
        }
    }
}