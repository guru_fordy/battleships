﻿using System.Collections.Generic;
using System.Linq;

namespace Battleships.Domain
{
    public abstract class Ship : ShotObserver
    {
        public List<GridReference> GridLocations { get; private set; }
        public int Health { get; private set; }
        public bool IsSunk
        {
            get
            {
                return Health == 0;
            }
        }

        public ShipDirection ShipDirection { get; private set; }
        public ShipType ShipType { get; set; }
        public int Size { get; private set; }
        public Ship(ShipDirection shipDirection, int size, ShipType shipType)
        {
            ShipDirection = shipDirection;
            Size = size;
            Health = size;
            ShipType = shipType;
            GridLocations = new List<GridReference>();
        }

        public bool IsPlaced()
        {
            return GridLocations.Count > 0;
        }

        public bool PlaceShipOnGrid(Grid grid, GridReference locationStart)
        {
            if (IsValidPlacement(grid, locationStart))
            {
                if (ShipDirection == ShipDirection.Horizontal)
                {
                    var locationEnd = new GridReference(GridReferencePlusShipSize(locationStart.XCoordinate), locationStart.YCoordinate);
                    for (var i = locationStart.XCoordinate; i <= locationEnd.XCoordinate; i++)
                    {
                        var gridReference = new GridReference(i, locationStart.YCoordinate);
                        GridLocations.Add(gridReference);
                        grid.SetGridReferenceStatus(gridReference, GridReferenceStatus.ShipPart);
                    }
                }
                else
                {
                    var locationEnd = new GridReference(locationStart.XCoordinate, GridReferencePlusShipSize(locationStart.YCoordinate));
                    for (var i = locationStart.YCoordinate; i <= locationEnd.YCoordinate; i++)
                    {
                        var gridReference = new GridReference(locationEnd.XCoordinate, i);
                        GridLocations.Add(gridReference);
                        grid.SetGridReferenceStatus(gridReference, GridReferenceStatus.ShipPart);
                    }
                }

                return true;
            }
            return false;
        }
        public override Ship UpdateAttackedGridRef(GridReference gridReference)
        {
            if (GridLocations.Any(c => c.XCoordinate == gridReference.XCoordinate
                                    && c.YCoordinate == gridReference.YCoordinate))
            {
                Health--;
                return this;
            }
            else
            {
                return null;
            }
        }

        private int GridReferencePlusShipSize(int coordinate)
        {
            // minus one for zero based index
            return coordinate + Size - 1;
        }

        private bool IsGridReferenceEmpty(Grid grid, GridReference gridReference)
        {
            return grid.GetCellStatus(gridReference) == GridReferenceStatus.Sea;
        }

        private bool IsValidPlacement(Grid grid, GridReference locationStart)
        {
            if (ShipDirection == ShipDirection.Horizontal)
            {
                var endXCoordinate = GridReferencePlusShipSize(locationStart.XCoordinate);
                for (int i = locationStart.XCoordinate; i < endXCoordinate; i++)
                {
                    var gridReference = new GridReference(i, locationStart.YCoordinate);
                    if (!IsGridReferenceEmpty(grid, gridReference))
                    {
                        return false;
                    }
                }
            }
            else
            {
                var endYCoordinate = GridReferencePlusShipSize(locationStart.YCoordinate);
                for (int i = locationStart.YCoordinate; i < endYCoordinate; i++)
                {
                    var gridReference = new GridReference(locationStart.XCoordinate, i);
                    if (!IsGridReferenceEmpty(grid, gridReference))
                    {
                        return false;
                    }
                    ;
                }
            }
            return true;
        }
    }
}