﻿namespace Battleships.Domain
{
    public class Destroyer : Ship
    {
        public Destroyer(ShipDirection shipDirection) : base(shipDirection, 4, ShipType.Destroyer)
        {
        }
    }
}