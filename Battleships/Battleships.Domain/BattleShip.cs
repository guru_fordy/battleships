﻿namespace Battleships.Domain
{
    public class Battleship : Ship
    {
        public Battleship(ShipDirection shipDirection) : base(shipDirection, 5, ShipType.Battleship)
        {
        }
    }
}