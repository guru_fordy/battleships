﻿using System;

namespace Battleships.Domain
{
    public class GridReference
    {
        public int XCoordinate { get; private set; }
        public int YCoordinate { get; private set; }

        public GridReference(int xCoordinate, int yCoordinate)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }

        public GridReference(char xCoordinateLetter, int yCoordinate)
        {
            var charAsDecimal = char.ToUpper(xCoordinateLetter);

            if (charAsDecimal < 65 || charAsDecimal > 90)
            {
                throw new ArgumentException("Not a valid XCoordinate");
            }

            XCoordinate = charAsDecimal - 65;
            YCoordinate = yCoordinate;
        }

        public override string ToString()
        {
            return $"{(char)(XCoordinate + 65)}:{YCoordinate}";
        }
    }
}