﻿namespace Battleships.Domain
{
    public enum ShipType
    {
        Destroyer,
        Battleship
    }
}