﻿namespace Battleships.Domain
{
    public interface IObserveShots
    {
        void UpdateAttackedGridRef(GridReference gridReference);
    }
}