﻿namespace Battleships.Domain
{
    public enum GridReferenceStatus
    {
        Sea = 0,
        ShipPart,
        HitOnShip,
        Missed,
        Sunk
    }
}