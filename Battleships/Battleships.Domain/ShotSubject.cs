﻿using System.Collections.Generic;

namespace Battleships.Domain
{
    public abstract class ShotSubject
    {
        private List<ShotObserver> _observers = new List<ShotObserver>();

        public void Attach(ShotObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(ShotObserver observer)
        {
            _observers.Remove(observer);
        }

        public Ship Notify(GridReference gridReference)
        {
            Ship ship = null;
            foreach (ShotObserver o in _observers)
            {
                var shipLocated = o.UpdateAttackedGridRef(gridReference);

                if(shipLocated != null)
                {
                    ship = shipLocated;
                }
            }
            return ship;
        }
    }
}