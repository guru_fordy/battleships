# Battleships

## To Run
Open source in and compile!

## Tech Requirements
Visual Studio (~2017)
DotNet 4.7.1

## Requirements
1. Create an application to allow a single human player to play a one-sided game of Battleships against ships placed by the computer.
2. The program should create a 10x10 grid, and place several ships on the grid at random with the following sizes:
    + 1x Battleship (5 squares)
    + 2x Destroyers (4 squares)
3. The player enters coordinates of the form “A5”, where "A" is the column and "5" is the row, to specify a square to target.
4.  Shots result in hits, misses or sinks. 
5.  The game ends when all ships are sunk.
6.  You can write a console application or UI to complete the task.

## Domain Extrapolation
+ Grid
    + Height
    + Width
    + GridReferences
    + Ships
+ GridReference
    + XCoordinate
    + YCoordinate
+ Ship (Battleship/Destroyer)
    + Size
    + GridLocations
    + Direction 
    + Health
    + IsSunk